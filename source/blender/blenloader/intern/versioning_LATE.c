/* SPDX-License-Identifier: GPL-2.0-or-later */

/** \file blender/blenloader/intern/versioning_upbge.c
 *  \ingroup blenloader
 */

#include "BLI_utildefines.h"
#include "BLI_compiler_attrs.h"
#include "BLI_listbase.h"

#include <stdio.h>

  /* allow readfile to use deprecated functionality */
#define DNA_DEPRECATED_ALLOW

#include "DNA_genfile.h"
#include "DNA_material_types.h"
#include "DNA_object_force_types.h"
#include "DNA_object_types.h"
#include "DNA_camera_types.h"
#include "DNA_sdna_types.h"
#include "DNA_sensor_types.h"
#include "DNA_space_types.h"
#include "DNA_view3d_types.h"
#include "DNA_screen_types.h"
#include "DNA_mesh_types.h"
#include "DNA_material_types.h"
#include "DNA_world_types.h"

#include "BKE_main.h"
#include "BKE_node.h"

#include "BLO_readfile.h"

#include "readfile.h"

#include "MEM_guardedalloc.h"

void blo_do_versions_LATE(FileData* fd, Library* lib, Main* main)
{
	if (!MAIN_VERSION_LATE_ATLEAST(main, 0, 2, 0)) {
		for (bScreen *screen = main->screen.first; screen; screen = screen->id.next) {
			for (ScrArea *sa = screen->areabase.first; sa; sa = sa->next) {
				for (SpaceLink *sl = sa->spacedata.first; sl; sl = sl->next) {
					if (sl->spacetype == SPACE_BUTS) {
						ListBase *regionbase = (sl == sa->spacedata.first) ? &sa->regionbase : &sl->regionbase;
						ARegion *ar = MEM_callocN(sizeof(ARegion), "navigation bar for properties");
						ARegion *ar_header = NULL;

						for (ar_header = regionbase->first; ar_header; ar_header = ar_header->next) {
							if (ar_header->regiontype == RGN_TYPE_HEADER) {
								break;
							}
						}
						BLI_assert(ar_header);

						BLI_insertlinkafter(regionbase, ar_header, ar);

						ar->regiontype = RGN_TYPE_NAV_BAR;
						ar->alignment = RGN_ALIGN_LEFT;
					}
				}
			}
		}
	}
  printf("LATE : Load File Version : %i.%i.%i\n", main->LATEversionmajorfile, main->LATEversionminorfile, main->LATEversionpatchfile);
}