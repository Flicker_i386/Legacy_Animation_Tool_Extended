![](doc/readme/LATE_banner.png)

**Legacy Animation Tool, Extended!** Or **LATE** Is A Free And Open-Source Animation Software Fork Of Legacy Blender 2.7 , Offering The Best Of Both World With Its Modern Design And New Features, Along With The Ultra Lightweight, Classic Stylized Real-time Render Engine.

LATE Is Forked From The Latest [**Legacy UPBGE**](https://upbge.org) Version Based From [**Legacy Blender 2.7**](https://blender.org/download/previous-versions/).
LATE Is In Mutual Collaboration With The Sibling Project [**RanGE**](https://rangeengine.tech) Game Engine. Both Project Trade Code And Feature To Improve Each Other Capabilities.

## Feature And Design

LATE Has Several New Animation Feature In Addition Of Blender Legacy Feature, Interface Design Is Based From Blender Update (2.8+).

## Minimum System Requirements

* **GPU Support OpenGL 2.1 (2006)**
  *Intel Or Nvidia GPU Is Recommended As AMD GPU Is Known To Have Graphical Issue.*
* **64 Bit Processor (2003)**
  *i386 (32 Bit) Support Is Dropped Due To Library Compatibility.*

## Contribution

LATE Is An Open-Source Software, And Contribution Are Always Welcome. You Can Contribute By Joining Our Public Group For Discussion, Using LATE For Your Projects, Or Just By Being Aware Of LATE's Existence.

## License

LATE Is Released Under The [GNU General Public License v3.0 Or Later](https://spdx.org/licenses/GPL-3.0-or-later.html).